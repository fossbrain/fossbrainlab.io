function randomNumber(min, max) { 
    return Math.floor(Math.random() * (max - min) + min);
} 

function getRandomImage() {
    path = '/images/grunge/'
    var num = randomNumber(1000, 1194);
    var img = path + num + '.jpg';
    return img;
}

document.addEventListener('DOMContentLoaded', (event) => {
    var gal = document.getElementById('grunge');
    for(var i = 0; i < 4; i++){
	var img = document.createElement("img");
	img.src = getRandomImage();
	img.alt = "Random Aesthetic Picsture";
	gal.appendChild(img);
    }
});
